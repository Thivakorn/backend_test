import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import Petinfo from "src/Pets_profile/PetInfo.entity";
import Userinfo from "src/User_info/Userinfo.entity";
import { UserinfoService } from "src/User_info/Userinfo.service";
import Newchat from "../Chat/newchat.entity";
import { NewChatService } from "../Chat/newchat.service";
import { RoomController } from "./newroom.controller";
import Newroom from "./newroom.entity";
import { NewRoomService } from "./newroom.service";

@Module({
    imports: [TypeOrmModule.forFeature([Newroom,Userinfo,Petinfo])],

    controllers: [RoomController],
    providers: [NewRoomService,UserinfoService]
})

export class NewRoomModule{}