import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { ObjectId, ObjectID } from "mongodb";
import { Repository } from "typeorm";
import { CreateNewRoomDto } from "../dto/create-newroom.dto";
import Newroom from "./newroom.entity";
import {getMongoRepository} from "typeorm";

@Injectable()
export class NewRoomService{
    constructor(
        @InjectRepository(Newroom)
        private NewRoomRepository: Repository<Newroom>,
    ) {}
    

    async createNewRoom(createNewRoomDto: CreateNewRoomDto){
        const roomexist = await this.getRoomByUserid(createNewRoomDto.userid1,createNewRoomDto.userid2)
        if(roomexist){
            throw new HttpException('Bad request',HttpStatus.BAD_REQUEST)
        }
        else{
            return this.NewRoomRepository.save(createNewRoomDto);
        }
    }

    async getAllRoom(UserId:ObjectID):Promise<Newroom[]>{
        const NewroomRepo = getMongoRepository(Newroom);
        const getAll = await NewroomRepo.find({
            where:{
                $or:[
                    {userid1: UserId},
                    {userid2: UserId}
                ]
            }
        })
        return getAll;
    }

    async getRoomByRoomid(RoomId: ObjectId): Promise<Newroom>{
        return this.NewRoomRepository.findOne({where:{id:RoomId}})
    }

    async getRoomByUserid(user1: ObjectID,user2: ObjectID): Promise<Newroom>{
        const RoomRepo = getMongoRepository(Newroom);
        const getRoom = await RoomRepo.findOne({
            where:{
                $or:[
                    {userid1:user1,userid2:user2},
                    {userid1:user2,userid2:user1}
                ]
            }
        })
        return getRoom;
    }

    // async isreadbyuser(UserId:ObjectID,roomid:ObjectID): Promise<Newroom>{
    //     const readornot = await this.getRoomByRoomid(roomid)
    //     readornot.isRead = false;
    //     await this.NewRoomRepository.save(readornot)

    //     return;
    // }
}

// async UpdateUserPhoneNO(UserId:ObjectID, PhoneNO: string): Promise<Userinfo>{
//     const userinfo = await this.findUserId(UserId)
//     userinfo.PhoneNo = PhoneNO;
//     await this.UserinfoRepository.save(userinfo);

//     return userinfo;
// }