import { Body, Controller, Get, Param, Patch, Post } from "@nestjs/common";
import { ObjectId, ObjectID } from "mongodb";
import { ParseObjectIdPipe } from "src/common/pipe";
import { UserinfoService } from "src/User_info/Userinfo.service";
import Newroom from "./newroom.entity";
import { NewRoomService } from "./newroom.service";
import { CreateNewRoomDto} from '../dto/create-newroom.dto'

@Controller('room')
export class RoomController{
    constructor(private NewRoomService: NewRoomService,
                private UserinfoService: UserinfoService){}

    @Post('createroom/:user1/:user2')
    async createnewroom(@Param('user1',ParseObjectIdPipe) user1: ObjectID,
                        @Param('user2',ParseObjectIdPipe) user2: ObjectID,
                        @Body() CreateNewRoomDto: CreateNewRoomDto){
        const username1 = await this.UserinfoService.findUserId(user1)
        const username2 = await this.UserinfoService.findUserId(user2)

        CreateNewRoomDto.userid1 = user1
        CreateNewRoomDto.userid2 = user2
        CreateNewRoomDto.username1 = username1.UserName
        CreateNewRoomDto.username2 = username2.UserName
        
        return this.NewRoomService.createNewRoom(CreateNewRoomDto);
    }

    @Get(':UserId/getAllRoom')
    async getAllRoom(@Param('UserId',ParseObjectIdPipe) UserId: ObjectID): Promise<Newroom[]>{
        return this.NewRoomService.getAllRoom(UserId);
    }

    @Get(':roomId/getRoom')
    async getRoomByRoomid(@Param('roomId',ParseObjectIdPipe) roomId: ObjectId): Promise<Newroom>{
        return this.NewRoomService.getRoomByRoomid(roomId);
    }

    @Get(':user1/:user2/getRoom')
    async getRoomByUserId(@Param('user1',ParseObjectIdPipe) user1: ObjectID,
                          @Param('user2',ParseObjectIdPipe) user2: ObjectID): Promise<Newroom>{
        return this.NewRoomService.getRoomByUserid(user1,user2);
    }

    // @Patch(':UserId/:roomid/isread')
    // async isreadbyuser(@Param('UserId') UserId: ObjectID,
    //                    @Param('roomid') roomid: ObjectID,): Promise<Newroom>{
    //     return this.NewRoomService.isreadbyuser(UserId,roomid);
    // }
}

// @Patch(':UserId/setting/email')
//     async UpdateUserEmail(@Param('UserId',ParseObjectIdPipe) UserId: ObjectID,
//                           @Body('Email') Email: string): Promise<Userinfo>{
//         return this.UserinfoService.UpdateUserEmail(UserId,Email);
//     }

// @Post('createroom/:receiverId/:senderId')
    // async createroom(@Param('receiverId',ParseObjectIdPipe) receiverId: ObjectID,
    //                  @Param('senderId',ParseObjectIdPipe) senderId: ObjectID,
    //                  @Body() CreateNewRoomDto:CreateNewRoomDto ){
    //     const UsernameReceiver = await this.UserinfoService.findUserId(receiverId)
    //     const UsernameSender = await this.UserinfoService.findUserId(senderId)
    //     const member:ObjectID[] = []
    //     const roomname:string[] = []

    //     member.push(senderId)
    //     member.push(receiverId)
        
    //     roomname.push(UsernameSender.UserName)
    //     roomname.push(UsernameReceiver.UserName)

    //     CreateNewRoomDto.bothUserId = member;
    //     CreateNewRoomDto.bothRoomName = roomname;

    //     return this.NewRoomService.createRoom(CreateNewRoomDto);
    // }