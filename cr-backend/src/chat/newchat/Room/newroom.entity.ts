import {Entity,Column,ObjectIdColumn}  from 'typeorm';
import { ObjectID} from 'mongodb';

@Entity()
export class Newroom{
    @ObjectIdColumn()
    id?: ObjectID;

    @Column()
    userid1 : ObjectID;

    @Column()
    userid2 : ObjectID;

    @Column()
    username1 : string;

    @Column()
    username2 : string;

}

export default Newroom;