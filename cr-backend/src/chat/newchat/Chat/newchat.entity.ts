import {Entity,Column,ObjectIdColumn}  from 'typeorm';
import { ObjectID} from 'mongodb';

@Entity()
export class Newchat{
    @ObjectIdColumn()
    id?: ObjectID;

    @Column()
    message: string;

    @Column()
    ownerName: string;

    @Column()
    ownerId: ObjectID;

    @Column()
    roomId: ObjectID;

    @Column()
    createAt: Date;

    @Column()
    readAt: Date;
}

export default Newchat;