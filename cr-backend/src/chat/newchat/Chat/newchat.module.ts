import { Controller, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import Petinfo from "src/Pets_profile/PetInfo.entity";
import Userinfo from "src/User_info/Userinfo.entity";
import { UserinfoService } from "src/User_info/Userinfo.service";
import Chatnoti from "../chatnotification.entity";
import Newroom from "../Room/newroom.entity";
import { ChatController } from "./newchat.controller";
import Newchat from "./newchat.entity";
import { NewChatService } from "./newchat.service";


@Module({
    imports: [TypeOrmModule.forFeature([Newroom,Newchat,Userinfo,Chatnoti,Petinfo])],

    controllers: [ChatController],
    providers: [NewChatService,UserinfoService]
})

export class NewChatModule{}