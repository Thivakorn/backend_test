import { Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";
import { ObjectID } from "mongodb";
import { ParseObjectIdPipe } from "src/common/pipe";
import { UserinfoService } from "src/User_info/Userinfo.service";
import { chatnotiDto } from "../chatnotification.dto";
import Chatnoti from "../chatnotification.entity";
import { CreateNewChatDto } from "../dto/create-newchat.dto";
import Newchat from "./newchat.entity";
import { NewChatService } from "./newchat.service";

@Controller('newchat')
export class ChatController{
    constructor(private NewChatService: NewChatService,
                private UserinfoService: UserinfoService){}

    @Post(':UserId/:roomId/addmessage')
    async addMessage(@Param('UserId',ParseObjectIdPipe) UserId: ObjectID,
                     @Param('roomId',ParseObjectIdPipe) roomId : ObjectID,
                     @Body() CreateNewChatDto: CreateNewChatDto){
        const Username = await this.UserinfoService.findUserId(UserId)
        CreateNewChatDto.ownerName = Username.UserName
        CreateNewChatDto.createAt = new Date();
        CreateNewChatDto.ownerId = UserId;
        CreateNewChatDto.roomId = roomId;
        return this.NewChatService.addMessage(CreateNewChatDto);
    }

    @Get(':UserId/getusermessage')
    async getusermessage(@Param('UserId',ParseObjectIdPipe) UserId: ObjectID): Promise<Newchat>{
        return this.NewChatService.getMessage(UserId)
    }

    @Get(':roomId/getmessage')
    async getAllMessageFromRoom(@Param('roomId') roomId: ObjectID): Promise<Newchat[]>{
        return this.NewChatService.getAllMessageFromRoom(roomId);
    }

    @Get(':notificationid/getNoti')
    async getNotification(@Param('notification') notificationid: ObjectID): Promise<Chatnoti>{
        return this.NewChatService.findnotibyId(notificationid)
    }

    @Post(':roomId/:UserId/noti')
    async chatnot(@Param('roomId',ParseObjectIdPipe) roomId: ObjectID,
                  @Param('UserId',ParseObjectIdPipe) UserId: ObjectID,
                  @Body() chatnotiDto: chatnotiDto){
        chatnotiDto.NotiDate = new Date();
        chatnotiDto.User = UserId;
        chatnotiDto.roomid = roomId;
        return this.NewChatService.notichat(chatnotiDto)
    }

    @Get('getnoti')
    async getNoti(): Promise<Chatnoti[]>{
        return this.NewChatService.getNoti();
    }

    @Delete(':notiId/delete')
    deleteNoti(@Param('notiId') notiId: string): Promise<void>{
        return this.NewChatService.deletenoti(notiId);
    }
}