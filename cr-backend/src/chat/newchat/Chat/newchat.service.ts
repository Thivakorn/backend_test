import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { ObjectID } from "mongodb";
import { Repository } from "typeorm";
import { chatnotiDto } from "../chatnotification.dto";
import Chatnoti from "../chatnotification.entity";
import { CreateNewChatDto } from "../dto/create-newchat.dto";
import Newchat from "./newchat.entity";

@Injectable()
export class NewChatService{
    constructor(
        @InjectRepository(Newchat)
            private NewchatRepository: Repository<Newchat>,
        @InjectRepository(Chatnoti)
            private ChatNotiRepository: Repository<Chatnoti>
    ) {}

    async addMessage(createNewChatDto: CreateNewChatDto){
        return this.NewchatRepository.save(createNewChatDto);
    }
    
    async getMessage(UserId: ObjectID):Promise<Newchat>{
        return this.NewchatRepository.findOne({where:{ownerId: UserId}})
    }

    async getAllMessageFromRoom(roomid: ObjectID): Promise<Newchat[]>{
        return this.NewchatRepository.find({where:{roomId:roomid}})
    }

    async findnotibyId(notificationid : ObjectID):Promise<Chatnoti>{
        return this.ChatNotiRepository.findOne({where:{id:notificationid}})
    }

    // async getNotibyUserAndSender(User:ObjectID,Sender:string):Promise<Chatnoti>{
    //     return this.ChatNotiRepository.findOne({where:{User:User,sender:Sender}})
    // }

    async getNotibyRoomidAndUser(User:ObjectID,roomId:ObjectID):Promise<Chatnoti>{
        return this.ChatNotiRepository.findOne({where:{User:User,roomid:roomId}})
    }

    // async userread(chatnotiDto: chatnotiDto): Promise<Chatnoti>{
    //     const { User,senderid,sender,NotiDate,isread,unreadmessage} = chatnotiDto
    //     const findnoti = await this.getNotibyUserAndSender(User,sender);
    //     findnoti.isread = true;
    //     await this.ChatNotiRepository.save(findnoti);

    //     return findnoti;
    // }

    // async plusunreadmessage(notificationid: ObjectID): Promise<Chatnoti>{
    //     const findnoti = await this.findnotibyId(notificationid);
    //     findnoti.unreadmessage = findnoti.unreadmessage+1;
    //     await this.ChatNotiRepository.save(findnoti);

    //     return findnoti;
    // }

    // async chatnoti(chatnotidto: chatnotiDto){
    //     const exist = await this.getNotibyUserAndSender(chatnotidto.User,chatnotidto.sender)
    //     if(!exist){
    //         return this.ChatNotiRepository.save(chatnotidto)
    //     }
    //     else{
    //         throw new HttpException('Bad request',HttpStatus.BAD_REQUEST)
    //     }
    // }

    async getNoti(): Promise<Chatnoti[]>{
        return this.ChatNotiRepository.find()
    }

    async notichat(chatnotiDto: chatnotiDto): Promise<Chatnoti>{
        const exist = await this.getNotibyRoomidAndUser(chatnotiDto.User,chatnotiDto.roomid)
        if(!exist){
            return this.ChatNotiRepository.save(chatnotiDto)
        }
        else{
            throw new HttpException('Bad request',HttpStatus.BAD_REQUEST)
        }
    }

    async deletenoti(id:string): Promise<void>{
        await this.ChatNotiRepository.delete(id);
    }
}

// async deleteUserId(id:string): Promise<void>{
//     await this.UserinfoRepository.delete(id);
// }