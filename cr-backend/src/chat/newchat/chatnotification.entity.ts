import {Entity,Column,ObjectIdColumn}  from 'typeorm';
import { ObjectID} from 'mongodb';

@Entity()
export class Chatnoti{
    @ObjectIdColumn()
    id?: ObjectID;

    @Column()
    User: ObjectID;

    @Column()
    roomid: ObjectID;

    @Column()
    NotiDate: Date;
}

export default Chatnoti;