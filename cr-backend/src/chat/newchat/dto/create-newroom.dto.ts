import { ObjectID } from "mongodb";

export class CreateNewRoomDto{
    userid1: ObjectID;

    userid2: ObjectID;

    username1: string; 

    username2: string;
}