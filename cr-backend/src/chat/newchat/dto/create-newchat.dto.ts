import { IsDate, isNotEmpty, IsNotEmpty } from "class-validator";
import { ObjectID } from "mongodb";

export class CreateNewChatDto{
    
    ownerId: ObjectID;

    ownerName: string;

    roomId: ObjectID;

    @IsNotEmpty()
    message: string;

    createAt: Date;

    readAt: Date;
}