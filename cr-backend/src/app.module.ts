import { Module } from '@nestjs/common';
import { TypeOrmModule} from '@nestjs/typeorm';

import Petinfo from './Pets_profile/PetInfo.entity';
import { PetinfoModule } from './Pets_profile/PetInfo.module';
import Userinfo from './User_info/Userinfo.entity';
import { UserinfoModule } from './User_info/Userinfo.module';
import { HomePageModule} from './Homepage/HomePage.module';
import Newchat from './chat/newchat/Chat/newchat.entity';
import Newroom from './chat/newchat/Room/newroom.entity';
import { NewRoomModule } from './chat/newchat/Room/newroom.module';
import { NewChatModule } from './chat/newchat/Chat/newchat.module';
import Chatnoti from './chat/newchat/chatnotification.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: 'mongodb+srv://worker:LpHNPZwDA4a36EH@cluster0.4yw9h.azure.mongodb.net/johnjudDB?retryWrites=true&w=majority',
      port: 27017,
      username: 'worker',
      password: 'LpHNPZwDA4a36EH',
      database: 'tan',
      useNewUrlParser: true,
      synchronize: true,
      logging: true,
      entities: [Petinfo,Userinfo,Newchat,Newroom,Chatnoti],
    }),
    
    PetinfoModule,UserinfoModule,HomePageModule,NewRoomModule,NewChatModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

/*
my localhost

      type: 'mongodb',
      host: 'localhost',
      database: 'JJ',
      entities: [Petinfo, Userinfo],
      synchronize: true,

---------------------------------------------------------------------
Group database

      type: 'mongodb',
      url: 'mongodb+srv://worker:LpHNPZwDA4a36EH@cluster0.4yw9h.azure.mongodb.net/johnjudDB?retryWrites=true&w=majority',
      port: 27017,
      username: 'worker',
      password: 'LpHNPZwDA4a36EH',
      database: 'tan',
      useNewUrlParser: true,
      synchronize: true,
      logging: true,
      entities: [Petinfo,Userinfo],


*/